#!/bin/bash

docker kill gitlab-formanek
docker rm gitlab-formanek

docker kill jenkins-formanek
docker rm jenkins-formanek

docker kill artifactory-formanek
docker rm artifactory-formanek

docker kill sonarqube-formanek
docker rm sonarqube-formanek

docker network rm formanek-pipeline

