#!/bin/bash

# Download, so that it works faster later on
if [[ "$(docker images -q gitlab/gitlab-ce:latest 2> /dev/null)" == "" ]]; then
  docker pull gitlab/gitlab-ce
else
  echo "Not downloading GitLab, already exists."
fi

if [[ "$(docker images -q jenkins:latest 2> /dev/null)" == "" ]]; then
  docker pull jenkins
else
  echo "Not downloading Jenkins, already exists."
fi

if [[ "$(docker images -q jfrog-docker-reg2.bintray.io/jfrog/artifactory-oss:latest 2> /dev/null)" == "" ]]; then
  docker pull jfrog-docker-reg2.bintray.io/jfrog/artifactory-oss
else
  echo "Not downloading Artifactory, already exists."
fi

if [[ "$(docker images -q sonarqube:latest 2> /dev/null)" == "" ]]; then
  docker pull sonarqube
else
  echo "Not downloading SonarQube, already exists."
fi

if ! docker network ls | grep formanek-pipeline; then
  docker network create --driver bridge formanek-pipeline
fi

if ! docker ps | grep gitlab-formanek; then
  docker run -d \
    -h gitlab.127.0.0.1.xip.io \
    -p 1443:443 -p 1080:80 -p 1022:22 \
    --name gitlab-formanek \
    --restart always \
    --net formanek-pipeline \
    -v /srv/formanek/gitlab/config:/etc/gitlab \
    -v /srv/formanek/gitlab/logs:/var/log/gitlab \
    -v /srv/formanek/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest
else
  echo "Not launching GitLab, already runnung."
  echo "Want to re-launch? Use destroy.sh first."
fi

if ! docker ps | grep jenkins-formanek; then
  if [ ! -d /srv/formanek/jenkins/home ]; then
    sudo mkdir -p /srv/formanek/jenkins/home
    sudo chmod 777 /srv/formanek/jenkins/home
  fi
  docker run -d \
    -h jenkins.172.0.0.1.xip.io \
    -p 2080:8080 -p 2050:50000 \
    --name jenkins-formanek \
    --restart always \
    --net formanek-pipeline \
    -v /srv/formanek/jenkins/home:/var/jenkins_home \
    jenkins
  echo "********************************************************************************"
  echo "Jenkins initial admin password:"
  cat /srv/formanek/jenkins/home/secrets/initialAdminPassword
  echo "********************************************************************************"
else
  echo "Not launching Jenkins, already running."
  echo "Want to re-launch? Use destroy.sh first."
fi

if ! docker ps | grep artifactory-formanek; then
  ARTIFACTORY_HOME=/var/opt/jfrog/artifactory
  docker run -d \
    -h artifactory.127.0.0.1.xip.io \
    -p 3443:443 -p 3080:80 -p 3081:8081 \
    --name artifactory-formanek \
    --restart always \
    --net formanek-pipeline \
    -v /srv/formanek/artifactory/data:$ARTIFACTORY_HOME/data \
    -v /srv/formanek/artifactory/logs:$ARTIFACTORY_HOME/logs \
    -v /srv/formanek/artifactory/backup:$ARTIFACTORY_HOME/backup \
    -v /srv/formanek/artifactory/etc:$ARTIFACTORY_HOME/etc \
    jfrog-docker-reg2.bintray.io/jfrog/artifactory-oss
else
  echo "Not launching Artifactory, already runnung."
  echo "Want to re-launch? Use destroy.sh first."
fi

if ! docker ps | grep sonarqube-formanek; then
  docker run -d \
    -h sonarqube.127.0.0.1.xip.io \
    -p 4000:9000 -p 4092:9092 \
    --name sonarqube-formanek \
    --restart always \
    --net formanek-pipeline \
    -v /srv/formanek/sonarqube/data:/opt/sonarqube/data \
    sonarqube
else
  echo "Not launching SonarQube, already runnung."
  echo "Want to re-launch? Use destroy.sh first."
fi

